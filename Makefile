CC = bspcc
FLAGS = -O3 -g -Wall -ffast-math -march=native -mtune=native
LFLAGS = -lm
APPS = $(wildcard apps/*.c)
BINS = $(patsubst apps/%.c, bin/%, $(APPS))

all: $(BINS)

bin/bspedupack.o: src/bspedupack.c include/bspedupack.h
	$(CC) $(FLAGS) -c $< -o $@

%.o: apps/%.c 
	$(CC) $(FLAGS) -c $< -o $@

bin/%: %.o bin/bspedupack.o
	$(CC) $^ -o $@ $(LFLAGS)

clean:
	$(RM) bin/*

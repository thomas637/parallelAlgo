This is part of BSPedupack 2.0 (https://webspace.science.uu.nl/~bisse101/Book2/psc2
authored by Rob Bisseling), with an automatic Makefile. Programs were slightly altered
so they compile warning free.

When you write a new BSP program in /apps and include "../include/bspedupack.h"
the Makefile automatically compiles your program with some useful flags, and links bspedupack.c

So you can do

vi apps/myprogram.c
... write program ...
make
bsprun bin/myprogram

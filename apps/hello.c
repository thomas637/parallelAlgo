#include "../include/bspedupack.h"

int totalProcessors;

void bspHello()
{
    bsp_begin(totalProcessors);
    printf("Hello from processor %d/%d!\n", bsp_pid(), totalProcessors);
    bsp_end();
}

int main(int argc, char **argv)
{
    bsp_init(bspHello, argc, argv);

    if (argc != 2) {
        printf("Usage: this program takes one command-line argument, the "
                "number of processors.\n");
        return EXIT_FAILURE;
    }

    /* This is not safe, only P(0) has access to global variables 
     * according to the BSP standard, but works with multicorebsp as
     * it uses pthread's shared memory under the hood. Unfortunately 
     * there is no good way to pass commandline arguments to every 
     * processor, so to be careful, one should communicate it from P(0)
     * to the other processors. */
    totalProcessors = atoi(argv[1]);

    if (totalProcessors > bsp_nprocs()){
        printf("Sorry, only %d processors available.\n",
               bsp_nprocs());
        return EXIT_FAILURE;
    }

    bspHello();

    return EXIT_SUCCESS;
}
